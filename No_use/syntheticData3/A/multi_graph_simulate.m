clc
clear all
close all

%T = 603;
n = 65;


pairs_name = 'pairs_golden_standard';
self_name_input = 'self_golden_standard';

pairs_name_input = strcat(pairs_name, '0');



simulate_expression(3003, n, pairs_name_input, self_name_input, '0', 1, 'train');
simulate_expression(1003, n, pairs_name_input, self_name_input, '0', 2, 'valid');
simulate_expression(1003, n, pairs_name_input, self_name_input, '0', 3, 'test');

pairs_name_input = strcat(pairs_name, '1');
simulate_expression(1003, n, pairs_name_input, self_name_input, '1', 4, 'train');
simulate_expression(53, n, pairs_name_input, self_name_input, '1', 5, 'valid');
simulate_expression(53, n, pairs_name_input, self_name_input, '1', 6, 'test');

pairs_name_input = strcat(pairs_name, '2');
simulate_expression(1003, n, pairs_name_input, self_name_input, '2', 7, 'train');
simulate_expression(33, n, pairs_name_input, self_name_input, '2', 8, 'valid');
simulate_expression(33, n, pairs_name_input, self_name_input, '2', 9, 'test');


pairs_name_input = strcat(pairs_name, '3');
simulate_expression(1003, n, pairs_name_input, self_name_input, '3', 10, 'train');
simulate_expression(27, n, pairs_name_input, self_name_input, '3', 11, 'valid');
simulate_expression(27, n, pairs_name_input, self_name_input, '3', 12, 'test');