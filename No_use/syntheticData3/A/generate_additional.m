function [A] = generate_additional(number, proportion, A0, seed)

rng(seed)
A = A0;


%first layer to second layer
proportion1 = floor(proportion*0.1);
%second layer to third layer
proportion2 = floor(proportion*0.7);
%inside second layer
proportion3 = proportion - proportion1 - proportion2;




topLayer = 1:5;
middleLayer = 6:20;
bottomLayer = 21:65;

pairs_filename = strcat(strcat('pairs_golden_standard', number), '.mat');
%pairs_wid=fopen(pairs_filename, 'w');

%self_filename = strcat(strcat('self_golden_standard', number), '.mat');

%self_wid = fopen(self_filename, 'w');

B = zeros(size(A));


i = 1;
while i <= proportion1
    temp1 = randi(5);
    temp2 = randi(15)+5;
    if A(temp1, temp2) == 0
        A(temp1, temp2) = 1;
        B(temp1, temp2) = 1;
        i = i + 1;
    end
end


i = 1;
while i <= proportion3
    temp1 = randi(15)+5;
    temp2 = randi(15)+5;
    if (A(temp1, temp2) == 0) && (temp1 ~= temp2) 
        A(temp1, temp2) = 1;
        B(temp1, temp2) = 1;
        i = i + 1;
    end
end

i = 1;
while i <= proportion2
    temp1 = randi(15)+5;
    temp2 = randi(45)+20;
    if A(temp1, temp2) == 0
        A(temp1, temp2) = 1;
        B(temp1, temp2) = 1;
        i = i + 1;
    end
end

for i = size(A,1)
    A(i,i) = 0;
    B(i,i) = 0;
end


%load('self_golden_standard.mat')
load('pairs_golden_standard0.mat')




% self_B = zeros(0,4);
% array=sum(B);
% for i=1:length(array)
%     if array(i)==0
%         self_B(size(self_B,1)+1,:) = [i, i, 1, 0.95+0.05*rand()];
% %        fprintf(self_wid, '%d\t%d\t%d\t%f\n', i, i, 1, 0.95+0.05*rand());
%     end
% end


pairs_B = zeros(0,4);

for i=1:size(B,1)
    for j=1:size(B,2)
        if i~=j && B(i, j)~=0
            pairs_B(size(pairs_B,1)+1,:) = [i,j,randi(3), -1 + 2*rand()];
%            fprintf(pairs_wid, '%d\t%d\t%d\t%f\n', i, j, randi(3), -1+2*rand()); 
        end
    end
end

pairs = [pairs;pairs_B];
%imagesc(A)
%grid on
%colorbar
%save('groundtruth.mat', 'A')

%fclose(pairs_wid);
%fclose(self_wid);

%save(self_filename, 'self');
save(pairs_filename, 'pairs');
save(strcat(strcat('A', number), '.mat'), 'A')

end

