function [A] = generate_additional(number, proportion, A0, seed)

rng(seed)
A = A0;


%first layer to second layer
proportion1 = floor(proportion*0.1);
%second layer to third layer
proportion2 = floor(proportion*0.7);
%inside second layer
proportion3 = proportion - proportion1 - proportion2;




topLayer = 1:10;
middleLayer = 11:40;
bottomLayer = 41:130;

pairs_filename = strcat(strcat('pairs_golden_standard', number), '.mat');

self_filename = strcat(strcat('self_golden_standard', number), '.mat');

i = 1;
while i <= proportion1
    temp1 = randi(10);
    temp2 = randi(30)+10;
    if A(temp1, temp2) == 0
        A(temp1, temp2) = 1;
        i = i + 1;
    end
end


i = 1;
while i <= proportion3
    temp1 = randi(30)+10;
    temp2 = randi(30)+10;
    if (A(temp1, temp2) == 0) && (temp1 ~= temp2) 
        A(temp1, temp2) = 1;
        i = i + 1;
    end
end

i = 1;
while i <= proportion2
    temp1 = randi(30)+10;
    temp2 = randi(90)+40;
    if A(temp1, temp2) == 0
        A(temp1, temp2) = 1;
        i = i + 1;
    end
end 

for i = size(A,1)
    A(i,i) = 0;
end


self = zeros(0,4);
array=sum(A);
for i=1:length(array)
    if array(i)==0
        self(size(self,1)+1,:) = [i, i, 1, 0.95+0.05*rand()];
    end
end


pairs = zeros(0,4);

for i=1:size(A,1)
    for j=1:size(A,2)
        if i~=j && A(i, j)~=0
            pairs(size(pairs,1)+1,:) = [i,j,randi(5), -1 + 2*rand()];
        end
    end
end

%pairs = [pairs;pairs_B];
%imagesc(A)
%grid on
%colorbar
%save('groundtruth.mat', 'A')

%fclose(pairs_wid);
%fclose(self_wid);

save(self_filename, 'self');
save(pairs_filename, 'pairs');
save(strcat(strcat('A', number), '.mat'), 'A')

end

