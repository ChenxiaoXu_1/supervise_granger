clc
clear all
close all

n = 130;

A0 = generate_from_scratch(n, '0');
figure(1)
imagesc(A0)
grid on

edges0 = sum(sum(A0>0));

edges1 = floor(edges0*0.1);
%edges2 = floor(edges0*0.20);
%edges3 = floor(edges0*0.30);

A1 = generate_additional('1', 20, A0, 5555);
A2 = generate_additional('2', 10, A1, 6666);
A3 = generate_additional('3', 10, A2, 7777);


figure(2)
imagesc(A1)
grid on

figure(3)
imagesc(A2)
grid on

figure(4)
imagesc(A3)
grid on


