function [] = simulate_expression(T, n, pairwise_name, self_name, number, seed)
%SIMULATE_EXPRESSION 此处显示有关此函数的摘要
%   此处显示详细说明
load(pairwise_name);
%pairs = genvarname(pairswise_name);
load(self_name);
%self = self_name;

expression = zeros(n, T);

rng(seed) 
expression(1:n,1)=normrnd(0,1,n,1);
expression(1:n,2)=normrnd(0,1,n,1);
expression(1:n,3)=normrnd(0,1,n,1);
expression(1:n,4)=normrnd(0,1,n,1);
expression(1:n,5)=normrnd(0,1,n,1);


a = exp((1/6)*pi*1.0i);
b = exp(-(1/6)*pi*1.0i);
c = a+b;
d = a*b;

for i=6:T
    for j=1:n
        expression(j,i) = normrnd(0, 1);
        array = find(pairs(:,2)==j);

        if size(array, 1)==0
            index=find(self(:,2)==j);
            expression(j,i) = expression(j,i) +  atan((c*self(index, 4)*expression(j,i-1) + d * self(index,4) * self(index,4) * expression(j,i-5))^2);            

        else
            for k=1:size(array,1)
                previous_value = pairs(array(k),4)*expression(pairs(array(k),1), i-pairs(array(k), 3));              
                expression(j,i) =  expression(j,i) + log(previous_value^2) + tanh(previous_value*expression(j,i-pairs(array(k),3)));
            end
            
             if size(array,1) > 1
                previous_value1  = expression(pairs(array(1),1), i-pairs(array(1), 3));
                previous_value2  = expression(pairs(array(2),1), i-pairs(array(2), 3));
                expression(j,i) = expression(j,i) + sin((previous_value1*previous_value2)^2);
            end
        end
    end
end

%no noise
expression0 = expression(:,6:end);

[expression0] = standalization(expression0);


%figure(1)
%plot(expression0)

expression = expression0;
store_name = strcat(strcat('filter_norm_expression', number), '.mat');
save(store_name, 'expression');
end

