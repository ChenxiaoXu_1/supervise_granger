clc
clear all
close all

%T = 603;
n = 65;


pairs_name = 'pairs_golden_standard';
self_name = 'self_golden_standard';

pairs_name_input = strcat(pairs_name, '0');
self_name_input = strcat('self_golden_standard', '0');
simulate_expression(15000, n, pairs_name_input, self_name_input, '0', 1);

pairs_name_input = strcat(pairs_name, '1');
self_name_input = strcat('self_golden_standard', '1');
simulate_expression(15000, n, pairs_name_input, self_name_input, '1', 12);

pairs_name_input = strcat(pairs_name, '2');
self_name_input = strcat('self_golden_standard', '2');
simulate_expression(15000, n, pairs_name_input, self_name_input, '2', 123);

pairs_name_input = strcat(pairs_name, '3');
self_name_input = strcat('self_golden_standard', '3');
simulate_expression(15000, n, pairs_name_input, self_name_input, '3', 1234);
